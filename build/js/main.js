$(window).on('load', function () {
	hide_preload();
	$('a.anchor').bind("click", function(e){

		var offsetTop = $(window).width() < 992 ? 60 : 0;
	    e.preventDefault();

	    var target = this.hash;
	    var $target = $(target);

	    

		$('html, body').stop().animate({
	        'scrollTop': $target.offset().top - offsetTop
	    }, 900, 'swing', function () {
	        window.location.hash = target;
	    });

	    if( $(window).width() < 992 ){
	    	$('header .btn-mob-menu').removeClass('active');
			$('.main_menu').removeClass('active');
	    }

	    return false;
	});
});

$(document).ready(function(){

});

$(window).resize(function(){

});

//Functions
function hide_preload(){

	var $preloader = $('#page-preloader'),
	$spinner   = $preloader.find('.spinner');
	$spinner.fadeOut();
	$preloader.delay(350).fadeOut('slow');
  
}

//Placeholder
var oldPlaceholder = "";

$(document).on("focusin", "input,textarea", function(){
    oldPlaceholder = $(this).attr('placeholder');
    $(this).attr('placeholder',""); 

    if( $(this).val() != ""){
    	$(this).addClass('changed');
    } else {
    	$(this).removeClass('changed');
    }
});

$(document).on("focusout", "input,textarea", function(){
    $(this).attr('placeholder',oldPlaceholder); 
    
});

$(document).find("textarea").keydown(function(e) {

    if( $(this).val() != ""){
    	$(this).addClass('changed');
    } else {
    	$(this).removeClass('changed');
    }

});


$(document).on('click', '[data-object="modal"]', function(){

	var id = $(this).attr('data-target');
	open_pop(id);
	
});


$(document).on('click', '[data-modal="modal"] .close, .background-mask', function(){
	close_pop($(this).parents('.popup').attr('id'));
});


$(document).on('click', '[data-object="modal_fade"]', function(){

	var id = $(this).attr('data-target');
	open_pop_video(id);
	
});

function open_pop(id){	

	DisableScrollbar();

	$('#' + id).addClass('active'); 
	$('body').addClass('over');

	setTimeout( function() { 
		$('#' + id).find('.background-mask').addClass('active');
	},10);

	$('#' + id).focus();

}
function close_pop(id){

	setTimeout( function() { 
		EnableScrollbar();
	}, 700);
	
	
	$('#' + id).find('.body').fadeOut();
	setTimeout( function() { 
		$('#' + id).find('.background-mask').removeClass('active');
	}, 10);
	setTimeout( function() { 
		$('#' + id).removeClass('active'); 
		$('body').removeClass('over'); 
	}, 700);
	
}


$('.main-slider').slick({
	infinite: true,
	arrows: false,
	slidesToShow: 1,
	slidesToScroll: 1,
	dots: true,
	autoplay: true,
	autoplaySpeed: 3000,
	swipe: false
});

$('.slider-product__slider').each(function () {

	if( $(this) && $(this).find('.col-xl-3').length > 4 ) {

		$(this).slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			prevArrow: "<button type=\"button\" class=\"slick-prev\">›</button>",
			nextArrow: "<button type=\"button\" class=\"slick-next\">›</button>"
		});
	}
});
