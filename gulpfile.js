'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'), //Будет нужен для наблюдения за изменениями файлов.
    prefixer = require('gulp-autoprefixer'), //автоматически добавляет вендорные префиксы к CSS свойствам 
    rigger = require('gulp-rigger'), // Плагин позволяет импортировать один файл в другой простой конструкцией "//= footer.html"
    concat = require('gulp-concat'), //Склейка файлов
    browserSync = require("browser-sync").create(), //с помощью этого плагина мы можем легко развернуть локальный dev сервер с блэкджеком и livereload, а так же с его помощью мы сможем сделать тунель на наш localhost, что бы легко демонстрировать верстку заказчику
    uglify = require('gulp-uglify'), //minify js
    cssmin = require('gulp-cssmin'),
    rename = require('gulp-rename'),
    pagebuilder = require('gulp-pagebuilder'),
    sass = require('gulp-sass');

var path = {

    build: {
        html: 'build/',
        css: 'build/css/',
        cssmin: 'build/css/min/',
        js: 'build/js/',
        jsmin: 'build/js/min/'
    },
    src: {
        html: 'src/html/**/*.html',
        style: 'src/style/*.scss',
        js: 'src/js/**/*.js'
    },
    watch: {
        html: 'src/html/**/*.html',
        style: 'src/style/**/*.scss',
        js: 'src/js/**/*.js'
    }
};

var config = {
    server: {
        baseDir: "./build"
    },
    tunnel: false,
    host: 'localhost',
    port: 3000,
    logPrefix: "Frontend"
};

gulp.task('webserver', function () {
    browserSync.init(config);

    gulp.watch(path.watch.html).on('change', gulp.parallel('html:build', browserSync.reload) );
    gulp.watch(path.watch.style).on('change', gulp.parallel('style:build', browserSync.reload) );
    gulp.watch(path.watch.js).on('change', gulp.parallel('js:build', browserSync.reload) );
});

gulp.task('html:build', function () {
    gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(pagebuilder('src'))
        .pipe(gulp.dest(path.build.html))
        .pipe(browserSync.stream());
});

gulp.task('style:build', function () {
    gulp.src(path.src.style)
        .pipe(sass().on('error', sass.logError))
        .pipe(prefixer())
        .pipe(concat('main.css'))
        .pipe(gulp.dest(path.build.css))
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(path.build.cssmin))
        .pipe(browserSync.stream());
});


gulp.task('js:build', function () {
    gulp.src(path.src.js)
        .pipe(concat('main.js'))
        .pipe(gulp.dest(path.build.js))
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(path.build.jsmin))
        .pipe(browserSync.stream());
});

gulp.task('build', gulp.series(
    'html:build',
    'style:build',
    'js:build'
));


gulp.task('default', gulp.parallel('build', 'webserver'));
